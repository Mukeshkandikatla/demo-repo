import React,{useEffect,useState} from "react";

const App =() => {
const [email,setemail] = useState('');
const [password,setpassword] = useState('');

useEffect(() => {
   fetch('https://fir-api-6e1b1-default-rtdb.firebaseio.com/data.json')
      .then((response) => response.json())
      .then((data) => {
         console.log(data);
      
      })
      .catch((err) => {
         console.log(err.message);
      });
}, []);
const changeHandler = e =>{
  setemail({email,[e.target.name]:e.target.value});
  setpassword({password,[e.target.name]:e.target.value})
}

const handleSubmit = (e) => {
  e.preventDefault();
};    


return(
  <div>
    <center>
      <form onSubmit={handleSubmit}>
        <label>E-mail</label> <br />
        <input type="text"  name="email" value={email} onChange={changeHandler} /> <br/>
        <label>Password</label> <br />
        <input type="password" name="password" value={password} onChange={changeHandler}/> <br/>
        <input type ="submit" value="Login" /> 
      </form>
    </center>
  </div>
 )
}

export default App;